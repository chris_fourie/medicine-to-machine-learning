# Medicine to Machine Learning 

**General structure** <br>
General resources <br>
Group <br>
Topics in group with specific resources

## Tools to teach

- Screen recording - [loom](https://www.loom.com/)
- Screen shots - [gyazo](https://gyazo.com/en)
- Screen share / pair programming - [teamviewer](https://www.teamviewer.com/en/)

## General  
**Finding text books** 
- [OpenSyllabus](https://opensyllabus.org/results-list/titles?size=50&findWorks=linear)

## Machine Learning 
- Textbook - [Dive into Deep Learning](https://d2l.ai)
- MOOC - [AI for Medicine](https://www.deeplearning.ai/ai-for-medicine/)
- Course - [Coursera AI specialization course:](https://www.deeplearning.ai/deep-learning-specialization/)

## Coding

- [Python QuickStart](https://www.youtube.com/watch?v=N4mEzFDjqtA)
- Get familiar with [colab](https://colab.research.google.com/drive/10DoCSnW0ICgUBAjJ6APQpwFpj1ygur5g)
- [DataCamp](https://www.datacamp.com/)
- [FreeCodeCamp.org](https://www.freecodecamp.org/)
- [TechDevGuide by Google](https://techdevguide.withgoogle.com/resources/?no-filter=true)

### Install Linux - Dual booting
#### MacOS

- TODO @Benji 
- https://www.howtogeek.com/187410/how-to-install-and-dual-boot-linux-on-a-mac/
- https://ubuntu.com/download/desktop/thank-you?version=18.04.4&architecture=amd64

#### Windows
 

## Math

### Getting started 

[Khanacademy.org](https://www.khanacademy.org/)

[Brilliant.org](https://brilliant.org/)

[Mathematics for machine learning](https://www.coursera.org/specializations/mathematics-machine-learning#courses)

### Linear Algebra
- Textbook - [Linear Algebra and Its Applications](https://opensyllabus.org/result/title?id=292057941208)

### Calculus

- Calculus playlist - [3Blue1Brown(Grant Sanderson)](https://www.youtube.com/watch?v=WUvTyaaNkzM&list=PLZHQObOWTQDMsr9K-rj53DwVRMYO3t5Yr)
 

### Multi-variable Calculus



